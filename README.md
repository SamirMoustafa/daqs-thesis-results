# Differential Automated Quantization Search Thesis Results

This repository is to provide the master theis results for DAQS: Differential Automated Quantization.

### Master Thesis Link
Samir's mater theis link can be found here: http://samirmoustafa.ml/master_thesis/

#### How to View the Learning Curves

1. Download the repo using FTP port because it has file with large size 

    `wget https://gitlab.com/SamirMoustafa/differential-automated-quantization-search-thesis-results/-/archive/main/differential-automated-quantization-search-thesis-results-main.zip`

2. Uncompress the downloaded file 

    `unzip differential-automated-quantization-search-thesis-results-main.zip && cd differential-automated-quantization-search-thesis-results-main/`

3. Install the requirement packages for python 3.7 

    `pip install -r requirements.txt`

4. Uncompress the results file 

    `unzip mini_thesis_results.zip`

5. Start the tensorboard for visualization 

    `tensorboard --logdir=./ --host=0.0.0.0`



#### Results Visualization
Architecture\ Datset  | CIFAR10 | ImageNet
---                   | ---     | ---
MobileNetV2+ROM       | ![](./thesis_visualization/accuracy_vs_bit-width/images/mobilenet_v2_cifar10_rom.jpg) | ![](./thesis_visualization/accuracy_vs_bit-width/images/mobilenet_v2_imagenet100_rom.jpg)
MobileNetV2+Latency   | ![](./thesis_visualization/accuracy_vs_bit-width/images/mobilenet_v2_cifar10_relaxed_latency.jpg) | -
ResNet50+ROM          | ![](./thesis_visualization/accuracy_vs_bit-width/images/resnet50_cifar10_rom.jpg) | -
ResNet18+ROM          | ![](./thesis_visualization/accuracy_vs_bit-width/images/resnet18_cifar10_rom.jpg) | -
ResNet18+Latency      | ![](./thesis_visualization/accuracy_vs_bit-width/images/resnet18_cifar10_relaxed_latency.jpg) | -
